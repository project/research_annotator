<?php
/**
 * @file: Functionality for the research annotator module.
 */

/**
 * Implements hook_permission().
 */
function research_annotator_permission() {
  return array(
    'research_annotator_create_annotation' => array(
      'title' => t('Create Annotations'),
    ),
    'research_annotator_edit_own_annotation' => array(
      'title' => t('Edit own Annotations'),
    ),
    'research_annotator_edit_any_annotation' => array(
      'title' => t('Edit any Annotations'),
    ),
    'research_annotator_delete_own_annotation' => array(
      'title' => t('Delete own Annotations'),
    ),
    'research_annotator_delete_any_annotation' => array(
      'title' => t('Delete any Annotations'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function research_annotator_entity_info() {
  $arr_entityInfo = array(
    'research_annotation' => array(
      'label' => t('Research Annotation'),
      'plural label' => t('Research Annotations'),
      'description' => t('An annotation for the body field of a node.'),
      'controller class' => 'EntityAPIController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'research_annotator_annotation',
      'fieldable' => FALSE,
      'uri callback' => 'research_annotator_uri',
      'entity keys' => array(
        'id' => 'annotation_id',
        'label' => 'Annotation Id',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
      ),
      'module' => 'research_annotator',
    ),
  );

  return $arr_entityInfo;
}

/**
 * Uri callback for annotation entities.
 */
function research_annotator_uri($entity) {
  return array(
    'path' => 'annotation/' . $entity->annotation_id,
  );
}

/**
 * Implements hook_entity_property_info().
 */
function research_annotator_entity_property_info() {
  
  $arr_propertyInfo = array();
  
  $arr_propertyInfo['research_annotation'] = array(
    'properties' => array(
      'node_id' => array(
        'label' => t('Related Node'),
        'type' => 'integer',
        'description' => t('The revision id this annotation belongs to.'),
        'setter callback' => 'entity_property_verbatim_set',
        'required' => TRUE,
        'schema field' => 'node_id',
      ),
      'uid' => array(
        'label' => t('Author'),
        'type' => 'integer',
        'description' => t('The author of the annotation.'),
        'setter callback' => 'entity_property_verbatim_set',
        'required' => TRUE,
        'schema field' => 'uid',
      ),
      'paragraph_index' => array(
        'label' => t('Paragraph Index'),
        'type' => 'integer',
        'description' => t('The element number the annotation belongs to.'),
        'setter callback' => 'entity_property_verbatim_set',
        'required' => FALSE,
        'schema field' => 'paragraph_index',
      ),
      'annotation' => array(
        'label' => t('Annotation'),
        'type' => 'text',
        'description' => t('The annotation content.'),
        'setter callback' => 'entity_property_verbatim_set',
        'required' => FALSE,
        'schema field' => 'annotation',
      ),
      'annotation_format' => array(
        'label' => t('Annotation Format'),
        'type' => 'text',
        'description' => t('The text format of the annotation.'),
        'setter callback' => 'entity_property_verbatim_set',
        'required' => FALSE,
        'schema field' => 'annotation_format',
      ),
      'created' => array(
        'label' => t('Date created'),
        'type' => 'date',
        'description' => t('The date the annotation was created.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'created',
      ),
      'updated' => array(
        'label' => t('Date updated'),
        'type' => 'date',
        'description' => t('The date the annotation was updated.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'updated',
      )
    ),
  );
  
  return $arr_propertyInfo;
}

/**
 * Implements hook_form_alter().
 */
function research_annotator_form_alter(&$form, &$form_state, $form_id) {

  // Editing the manage field forms for field_body.
  if ($form_id == 'field_ui_field_edit_form' && $form['#field']['field_name'] == 'body') {
    
    // Include some functions that we will need.
    module_load_include('inc', 'research_annotator', 'inc/research_annotator.admin');

    // Prep for getting default form values.
    $s_fieldIdentifier = $form['#instance']['field_name'] . '_' . $form['#instance']['bundle'];

    // Add new form components.
    $form['research_annotator_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Research Annotation Settings'),
      '#description' => t(
        'NOTE: The text format used on this field MUST allow paragraph tags for annotations to work.<br/>
        You can use the <a href="@format_link" target="_blank">Annotation Safe Output</a> 
        text format on this body field to ensure your annotations will work.',
        array(
          '@format_link' => '/admin/config/content/formats/annotation_safe_output'
        )
      ),
      '#tree' => TRUE,
    );
    
    $form['research_annotator_settings']['research_annotator_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable annotations on this field'),
      '#default_value' => variable_get('research_annotator_' . $s_fieldIdentifier . '_enabled', 0),
    );
    
    $form['research_annotator_settings']['research_annotator_date_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Date Format'),
      '#description' => t('The date format you want for the annotations that are displayed. Must be a valid PHP date format.'),
      '#default_value' => variable_get('research_annotator_' . $s_fieldIdentifier . '_date_format', ''),
      '#size' => 15,
    );
    
    $form['research_annotator_settings']['research_annotator_block_region'] = array(
      '#type' => 'select',
      '#title' => t('Available Block Region'),
      '#default_value' => variable_get('research_annotator_' . $s_fieldIdentifier . '_block_region', '0'),
      '#options' => research_annotator_get_block_regions(),  
    );
    
    $form['research_annotator_settings']['research_annotator_block_weight'] = array(
      '#type' => 'select',
      '#title' => t('Weight'),
      '#description' => t('Determines where annotations will appear in the block region'),
      '#default_value' => variable_get('research_annotator_' . $s_fieldIdentifier . '_block_weight', 0),
      '#options' => array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3', 
        4 => '4',  
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
      ),  
    );
    
    $form['research_annotator_settings']['research_annotator_cache_settings'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable page caching on the annotated page.'),
      '#description' => t('Check this if you have page caching enabled and you want anonymous users to see changes to annotations right after they occur.'),
      '#default_value' => variable_get('research_annotator_' . $s_fieldIdentifier . '_cache_disabled', 0),
    );
        
    // Add any custom handlers.
    $form['#validate'][] = 'research_annotator_validate';
    $form['#submit'][] = 'research_annotator_submit';
  }
  
  // Editing the node edit forms where field_body is present.
  elseif ($form_id == 'page_node_form' && isset($form['body']) && variable_get('research_annotator_body_' . $form['type']['#value'] . '_enabled', FALSE)) {
    
    if (user_access('research_annotator_delete_own_annotation') || user_access('research_annotator_delete_any_annotation')) {
      
      // Include some files we will need.
      module_load_include('inc', 'research_annotator', 'inc/research_annotator.admin');
      drupal_add_css(drupal_get_path('module', 'research_annotator') . '/css/research_annotator_admin.css');
      
      $query = new EntityFieldQuery();
      $result = array();
    
      // Determine if user has delete permissions.
      if (user_access('research_annotator_delete_any_annotation')) {

        // Get annotations and format as options.
        $result = $query
                  ->entityCondition('entity_type', 'research_annotation')
                  ->propertyCondition('node_id', $form['nid']['#value'])
                  ->execute();
      }
      elseif (user_access('research_annotator_delete_own_annotation')) {

        global $user;
        $result = $query
                  ->entityCondition('entity_type', 'research_annotation')
                  ->propertyCondition('node_id', $form['nid']['#value'])
                  ->propertyCondition('uid', $user->uid, '=')
                  ->execute();
      }

      $arr_annotationIds = array();

      if (!empty($result)) {
        foreach ($result['research_annotation'] as $o_annotationId) {
          $arr_annotationIds[] = $o_annotationId->annotation_id;
        }
      }

      $arr_annotations = entity_load('research_annotation', $arr_annotationIds);
      $arr_options = array();

      foreach ($arr_annotations as $o_annotation) {
        $arr_options[$o_annotation->annotation_id] = check_markup($o_annotation->annotation, $o_annotation->annotation_format);
      }

      // Add a new form field.
      $form['body']['research_annotations'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Annotations Kept in this Revision'),
        '#description' => t('Uncheck the following annotations if they are not applicable and need to be deleted.') . '<br/>' .
                          '<strong>' . t('Note: Annotations are not recoverable after you delete them!') . '</strong>',
        '#options' => $arr_options,
        '#default_value' => array_keys($arr_options),
        '#weight' => 50,
      );

      $form['#submit'][] = 'research_annotator_update_kept_revisions';
    }
  } 
}


/**
 * Implements hook_forms().
 */
function research_annotator_forms($form_id, $args) {
  return array(
    'annotation_creation' => array(
      'callback' => 'research_annotator_generate_creation_form',
    ),
    'annotation_edit' => array(
      'callback' => 'research_annotator_generate_edit_form',
      'callback arguments' => array(-1, -1, '', 'plain', '', ),
    ),
    'annotation_delete' => array(
      'callback' => 'research_annotator_generate_delete_form',
      'callback arguments' => array(-1, '', '', ),
    )
  );
}

/**
 * Form callback.
 * Generates an annotation creation form.
 */
function research_annotator_generate_creation_form() {
  
  // Include some functions that we will need.
  module_load_include('inc', 'research_annotator', 'inc/research_annotator_creation_form');
  
  // Setting up needed variables.
  $arr_form = array();

  // Create the annotation form form.
  $arr_form['annotation_body'] = array(
    '#type' => 'text_format',
    '#title' => t('Annotation'),
    '#default_value' => '',
    '#required' => TRUE,
    '#weight' => 0,
  );
  
  $arr_form['paragraph_index'] = array(
    '#type' => 'hidden',
  );
  
  $arr_form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Annotation'),
  );
  
  $arr_form['#validate'][] = 'research_annotator_validate_annotation_creation';
  $arr_form['#submit'][] = 'research_annotator_submit_annotation_creation';
  
  return $arr_form;
}

/**
 * Form Callback.
 * Generates an annotation edit form.
 */
function research_annotator_generate_edit_form($form_id, $args) {
  
  // Include some functions that we will need.
  module_load_include('inc', 'research_annotator', 'inc/research_annotator_edit_form');

  // Setting up needed variables.
  $arr_form = array();

  // Create form.
  $arr_form['annotated_node_body'] = array(
    '#type' => 'markup',
    '#markup' => $args['build_info']['args'][4],
    '#prefix' => '<div class="annotation-content-edit">',
    '#suffix' => '</div>',
  );
  
  $arr_form['annotation_body'] = array(
    '#type' => 'text_format',
    '#title' => t('Annotation'),
    '#default_value' => $args['build_info']['args'][2],
    '#required' => TRUE,
    '#weight' => 0,
    '#format' => $args['build_info']['args'][3],
  );
  
  $arr_form['annotation_id'] = array(
    '#type' => 'hidden',
    '#value' => $args['build_info']['args'][0],
    '#required' => TRUE,
  );
  
  $arr_form['paragraph_index'] = array(
    '#type' => 'hidden',
    '#value' => $args['build_info']['args'][1],
    '#required' => TRUE,
  );
  
  $arr_form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit Annotation'),
  );

  $arr_form['#validate'][] = 'research_annotator_validate_edited_annotation';
  $arr_form['#submit'][] = 'research_annotator_submit_edited_annotation';
  
  return $arr_form;
}

/**
 * Form Callback.
 * Generates an annotation deletion form.
 */
function research_annotator_generate_delete_form($form_id, $args) {
  
  // Include some functions that we will need.
  module_load_include('inc', 'research_annotator', 'inc/research_annotator_delete_form');

  // Setting up needed variables.
  $arr_form = array();

  // Create form.
  $arr_form['annotation_body'] = array(
    '#type' => 'markup',
    '#markup' => $args['build_info']['args'][1],
    '#prefix' => '<div class="annotation-content-delete"><h3>' . t('Are you sure you want to delete the following annotation?') . '</h3>',
    '#suffix' => '</div>',
  );
  
  $arr_form['annotation_id'] = array(
    '#type' => 'hidden',
    '#value' => $args['build_info']['args'][0],
    '#required' => TRUE,
  );
  
  $arr_form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Annotation'),
  );
  
  $arr_form['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), $args['build_info']['args'][2]),
  );

  $arr_form['#validate'][] = 'research_annotator_validate_deleted_annotation';
  $arr_form['#submit'][] = 'research_annotator_submit_deleted_annotation';
  
  return $arr_form;
}

/**
 * Helper function.
 * Determines whether or not the current page should have annotations enabled on it.
 * @return: The current node IF annotations belong on it, NULL otherwise.
 */
function research_annotator_get_annotation_node() {
  
  $o_annotationNode = NULL;
  
  $i_nid = (arg(0) == 'node' && is_numeric(arg(1))) ? arg(1) : 0;
  
  if ($i_nid != 0) {
    $o_currentNode = node_load($i_nid);
    
    if (property_exists($o_currentNode, 'body') && variable_get('research_annotator_body_' . $o_currentNode->type . '_enabled') == 1) {
      $o_annotationNode = $o_currentNode;
    }
  }
  
  return $o_annotationNode;
}

/**
 * Implements hook_page_build().
 */
function research_annotator_page_build(&$page) {
  
  $o_currentNode = research_annotator_get_annotation_node();
  
  if ($o_currentNode != NULL) {
    
    // Set up the annotator block area.
    if (!isset($page[variable_get('research_annotator_body_' . $o_currentNode->type . '_block_region')])) {
      $page[variable_get('research_annotator_body_' . $o_currentNode->type . '_block_region')] = array();
    }

    $page[variable_get('research_annotator_body_' . $o_currentNode->type . '_block_region')]['research_annotator'] = array(
      '#markup' => theme('research_annotator_wrapper'),
      '#weight' => variable_get('research_annotator_body_' . $o_currentNode->type . '_block_weight'),
      '#sorted' => TRUE,
    );

    $page[variable_get('research_annotator_body_' . $o_currentNode->type . '_block_region')]['#sorted'] = FALSE;    
  }
}

/**
 * Implements hook_preprocess_page().
 */
function research_annotator_preprocess_page(&$variables) {
    
  $o_currentNode = research_annotator_get_annotation_node();
  
  if ($o_currentNode != NULL) {
    
    // Disable caching for annotated page if admin settings allow it.
    if (variable_get('research_annotator_body_' . $o_currentNode->type . '_cache_disabled', 0) == TRUE) {
      drupal_page_is_cacheable(FALSE);
    }
    
    // Add necessary frontend components.
    drupal_add_css(drupal_get_path('module', 'research_annotator') . '/css/research_annotator.css');
    drupal_add_js(drupal_get_path('module', 'research_annotator') . '/js/research_annotator.js');
        
  }
}

/**
 * Implements hook_preprocess_field().
 */
function research_annotator_preprocess_field(&$variables, $hook) {
  
  $o_currentNode = research_annotator_get_annotation_node();
  
  if ($o_currentNode != NULL && $variables['element']['#field_name'] == 'body') {
    $variables['classes_array'][] = 'annotated-body';
  }

}

/**
 * Implements hook_theme().
 */
function research_annotator_theme($existing, $type, $theme, $path) {
  
  return array(
    'research_annotator_wrapper' => array(
      'type' => 'module',
      'path' => drupal_get_path('module', 'research_annotator') . '/template',
      'template' => 'research-annotator-wrapper',
      'render element' => 'annotation_wrapper_variables',
    ),
    'research_annotator_form' => array(
      'type' => 'module',
      'path' => drupal_get_path('module', 'research_annotator') . '/template',
      'template' => 'research-annotator-form',
      'render element' => 'annotation_form_variables',
    ),
    'research_annotator_annotation' => array(
      'type' => 'module',
      'path' => drupal_get_path('module', 'research_annotator') . '/template',
      'template' => 'research-annotator-annotation',
      'variables' => array(
        'author_picture' => '',
        'author_name' => '',
        'markup' => '',
        'updated_date' => 0,
        'edit_link' => array(),
        'delete_link' => array(),
      )
    )
  );
  
}

/**
 * Template hook.
 * Prepares variables for the annotator wrapper template.
 * Also produces theme_preprocess_research_annotator_wrapper().
 */
function template_preprocess_research_annotator_wrapper(&$variables) {
  
  global $user;
  
  // Get the annotations for the template.
  $o_annotatedNode = research_annotator_get_annotation_node();
  
  $o_entityQuery = new EntityFieldQuery();
  $arr_result = $o_entityQuery
    ->entityCondition('entity_type', 'research_annotation')
    ->propertyCondition('node_id', (int) $o_annotatedNode->nid)
    ->execute();
  
  $arr_annotationIds = array();
  
  if (!empty($arr_result)) {
    $arr_result = $arr_result['research_annotation'];
    foreach ($arr_result as $o_annotationId) {
      array_push($arr_annotationIds, $o_annotationId->annotation_id);
    }
  }
  
  $arr_processedAnnotations = array();
  
  if (!empty($arr_annotationIds)) {
    $arr_annotations = entity_load('research_annotation', $arr_annotationIds, array(), TRUE);
    
    // Process the annotations and put them in a render array.
    $s_dateFormat = $variables['annotation_wrapper_variables']['date_format'] = variable_get(
      'research_annotator_body_' . $o_annotatedNode->type . '_date_format',
      'd-M-Y'
    );

    foreach ($arr_annotations as $o_annotation) {

      $arr_editLink = array();
      if (research_annotator_access_edit_page($o_annotation->annotation_id)) {
        $arr_editLink = array(
          '#type' => 'link',
          '#title' => t('Edit this annotation'),
          '#href' => 'annotation/' . $o_annotation->annotation_id . '/edit/',
        );
      }

      $arr_deleteLink = array();
      if (research_annotator_access_delete_page($o_annotation->annotation_id)) {
        $arr_deleteLink = array(
          '#type' => 'link',
          '#title' => t('Delete this annotation'),
          '#href' => 'annotation/' . $o_annotation->annotation_id . '/delete/',
        );
      }

      $arr_processedAnnotations[$o_annotation->paragraph_index][] = array(
        'annotation_author' => user_load($o_annotation->uid),
        'markup' => array(
          '#type' => 'markup',
          '#markup' => check_markup($o_annotation->annotation, $o_annotation->annotation_format),
        ),
        'updated_date' => date($s_dateFormat, $o_annotation->updated),
        'edit_link' => $arr_editLink,
        'delete_link' => $arr_deleteLink,
      );

    }
  
    ksort($arr_processedAnnotations);
  }
  
  
  // Set a renderable annotations array.
  $arr_templateAnnotations = array();
  
  foreach ($arr_processedAnnotations as $s_paragraphIndex => $arr_sectionAnnotations) {
    
    $s_annotationsHTML = '';
    
    foreach ($arr_sectionAnnotations as $arr_annotation) {
      $s_annotationsHTML .= theme(
        'research_annotator_annotation', 
        array(
          'author_picture' => theme('user_picture', array('account' => $arr_annotation['annotation_author'])), 
          'author_name' => (property_exists($arr_annotation['annotation_author'], 'name') &&  $arr_annotation['annotation_author']-> name != '' ? $arr_annotation['annotation_author']->name : t('Anonymous')),
          'markup' => $arr_annotation['markup'], 
          'updated_date' => $arr_annotation['updated_date'],
          'edit_link' => $arr_annotation['edit_link'],
          'delete_link' => $arr_annotation['delete_link'],
        )
      );
    }
        
    array_push(
      $arr_templateAnnotations,
      array(
        '#type' => 'markup',
        '#prefix' => '<div class="annotation-section annotation-section-minimized">',
        '#markup' =>  '<h3 trigger="toggle-annotation-display" id="' . $s_paragraphIndex . '">' .
                      t('Section @index annotations', array('@index' => $s_paragraphIndex)) .
                      '</h3>' .
                      '<div class="annotation-inner-section">' .
                      $s_annotationsHTML .
                      '</div>',
        '#suffix' => '</div>',
      )
    );
  }
  
  $variables['annotation_wrapper_variables']['annotations'] = $arr_templateAnnotations;

  // Set the form.
  if (user_access('research_annotator_create_annotation')) {
    $variables['annotation_wrapper_variables']['form'] = theme('research_annotator_form');
  }
  else {
    $variables['annotation_wrapper_variables']['form'] = '';
  }
  
  // Set the "no annotations" message.
  $variables['annotation_wrapper_variables']['no_annotations_msg'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="no-annotations">',
    '#markup' => t('This content has not been annotated yet.'),
    '#suffix' => '</span>',
  );
}

/**
 * Template hook.
 * Prepares variables for the annotator form template.
 * Also produces theme_preprocess_research_annotator_form().
 */
function template_preprocess_research_annotator_form(&$variables) {
  
  global $user;
  
  // Set current user picture.
  $variables['annotation_form_variables']['current_annotation_picture'] = theme('user_picture', array('account' => $user));
  
   // Set the annotation form for the template.
  $variables['annotation_form_variables']['annotation_form'] = drupal_get_form('annotation_creation');
  
  // Set up current author of annotation.
  $variables['annotation_form_variables']['current_annotation_author'] = array(
    '#type' => 'markup',
    '#prefix' =>  '<div class="author-info"><span>',
    '#markup' =>  '<strong>' . t('Posting as') . ': </strong>' . (property_exists($user, 'name') ? $user->name : t('Anonymous')) .
                  '</span>', 
    '#suffix' =>  '</span></div>',
  );
}

/**
 * Implements hook_menu().
 */
function research_annotator_menu() {
  return array(
    'annotation/%/edit' => array(
      'title' => 'Editing Annotation',
      'page callback' => 'research_annotator_edit_page',
      'page arguments' => array(1),
      'access callback' => 'research_annotator_access_edit_page',
      'access arguments' => array(1),
      'type' => MENU_NORMAL_ITEM,
    ),
    'annotation/%/delete' => array(
      'title' => 'Delete Annotation',
      'page callback' => 'research_annotator_delete_page',
      'page arguments' => array(1),
      'access callback' => 'research_annotator_access_delete_page',
      'access arguments' => array(1),
      'type' => MENU_NORMAL_ITEM,
    )
  );
}

/**
 * Page callback for the annotation edit page.
 * @param i_annotationId: The ID of the annotation being edited.
 * @return: A renderable Drupal array.
 */
function research_annotator_edit_page($i_annotationId) {

  // Add some js and css.
  drupal_add_css(drupal_get_path('module', 'research_annotator') . '/css/research_annotator.css');
  drupal_add_js(drupal_get_path('module', 'research_annotator') . '/js/research_annotator.js');
  
  // Prepare elements and forms.
  $arr_annotation = entity_load('research_annotation', array($i_annotationId));
  $o_annotation = $arr_annotation[$i_annotationId];
  $o_annotatedNode = node_load($o_annotation->node_id);
  
  drupal_set_title(t('Editing annotation for @node_title', array('@node_title' => $o_annotatedNode->title)));
    
  $arr_form = drupal_get_form(
    'annotation_edit', 
    $o_annotation->annotation_id,
    $o_annotation->paragraph_index,
    $o_annotation->annotation,
    $o_annotation->annotation_format,
    $o_annotatedNode->body['und'][0]['safe_value']
  );
  
  return $arr_form;
}

/**
 * Access callback for the annotation edit page.
 * @param i_annotationId: The ID of the annotation being edited.
 * @return: True if you're allowed to see the page, false otherwise.
 */
function research_annotator_access_edit_page($i_annotationId) {
  
  global $user;
  $b_hasPermission = FALSE;
  $o_annotation = array_pop(entity_load('research_annotation', array($i_annotationId)));
  
  if ($o_annotation != FALSE) {
    if (user_access('research_annotator_edit_any_annotation')) {
      $b_hasPermission = TRUE;
    }
    elseif ($o_annotation->uid == (int) $user->uid && user_access('research_annotator_edit_own_annotation')) {
      $b_hasPermission = TRUE;
    }
    elseif (in_array('administrator', $user->roles)) {
      $b_hasPermission = TRUE;
    }
  }
  
  return $b_hasPermission;
}

/**
 * Page callback for the annotation edit page.
 * @param i_annotationId: The ID of the annotation being edited.
 * @return: A renderable Drupal array.
 */
function research_annotator_delete_page($i_annotationId) {
  
  // Prepare elements and forms.
  $arr_annotation = entity_load('research_annotation', array($i_annotationId));
  $o_annotation = $arr_annotation[$i_annotationId];
  $o_annotatedNode = node_load($o_annotation->node_id);
    
  $arr_form = drupal_get_form(
    'annotation_delete', 
    $o_annotation->annotation_id,
    $o_annotation->annotation,
    drupal_get_path_alias('node/' . $o_annotatedNode->nid)
  );
  
  return $arr_form;
}

/**
 * Access callback for the annotation deletion page.
 * @return: True if you're allowed to see the page, false otherwise.
 */
function research_annotator_access_delete_page($i_annotationId) {

  global $user;
  $b_hasPermission = FALSE;
  $o_annotation = array_pop(entity_load('research_annotation', array($i_annotationId)));

  if ($o_annotation != FALSE) {
    if (user_access('research_annotator_delete_any_annotation')) {
      $b_hasPermission = TRUE;
    }
    elseif ($o_annotation->uid == (int) $user->uid && user_access('research_annotator_delete_own_annotation')) {
      $b_hasPermission = TRUE;
    }
    elseif (in_array('administrator', $user->roles)) {
      $b_hasPermission = TRUE;
    }
  }

  return $b_hasPermission;
}
